package com.kenfogel.navigation;

import java.io.Serializable;

/**
 * This is an ordinary bean that will be instantiated by the CDI managed
 * QuizBean. The bean is takes the on responsibility for deciding if an answer
 * is right or wrong
 *
 * @author Ken
 * @version 1.0
 *
 */
public class Problem implements Serializable {

    private final String question;
    private final String answer;

    public Problem(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    // override for more sophisticated checking
    public boolean isCorrect(String response) {
        return response.trim().equalsIgnoreCase(answer);
    }
}
