package com.kenfogel.navigation;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import javax.enterprise.context.RequestScoped;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

/**
 * This is a CDI managed bean that is both data and action. Using actions is the
 * preferred way to carry out business processes. Notice that the setters and
 * getters are not balanced.
 *
 * @author Ken
 * @version 1.0
 *
 */
@Named
@SessionScoped
public class QuizBean implements Serializable {

    private int currentProblem;
    private int tries;
    private int score;
    private String response = "";
    private String correctAnswer;

    // The problems are hard wired into the ArrayList.
    // In a real application they would come from a database.
    private ArrayList<Problem> problems = new ArrayList<>(Arrays.asList(
            new Problem(
                    "What trademarked slogan describes Java development? Write once, ...",
                    "run anywhere"),
            new Problem(
                    "What are the first 4 bytes of every class file (in hexadecimal)?",
                    "CAFEBABE"),
            new Problem(
                    "What does this statement print? System.out.println(1+\"2\");",
                    "12"),
            new Problem(
                    "Which Java keyword is used to define a subclass?",
                    "extends"),
            new Problem(
                    "What was the original name of the Java programming language?",
                    "Oak"),
            new Problem(
                    "Which java.time class describes a point in time?",
                    "LocalDateTime")));

    public String getQuestion() {
        return problems.get(currentProblem).getQuestion();
    }

    public String getAnswer() {
        return correctAnswer;
    }

    public int getScore() {
        return score;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String newValue) {
        response = newValue;
    }

    /**
     * An action that determines if a guess is right or wrong. The outcome is
     * the navigation rule to be followed.
     *
     * @return navigation rule
     */
    public String answerAction() {
        tries++;
        if (problems.get(currentProblem).isCorrect(response)) {
            score++;
            nextProblem();
            if (currentProblem == problems.size()) {
                return "done";
            } else {
                return "success";
            }
        } else if (tries == 1) {
            return "again";
        } else {
            nextProblem();
            if (currentProblem == problems.size()) {
                return "done";
            } else {
                return "failure";
            }
        }
    }

    /**
     * An action that re-initialized the game so you can play again
     *
     * @return the start-over navigation rule
     */
    public String startOverAction() {
        // Nice touch shuffling the problems
        Collections.shuffle(problems);
        currentProblem = 0;
        score = 0;
        tries = 0;
        response = "";
        return "startOver";
    }

    /**
     * Advance to the next problem
     */
    private void nextProblem() {
        correctAnswer = problems.get(currentProblem).getAnswer();
        currentProblem++;
        tries = 0;
        response = "";
    }
}
